import cma
import numpy as np
from environment import Mountain
from sklearn.preprocessing import StandardScaler
from sklearn.kernel_approximation import RBFSampler
from sklearn.pipeline import Pipeline


class MC_State_Value_Approximation:
    """ MC State-value Learning with Function Approximation
    """

    def __init__(self):
        """
        Init:
            gamma: discount factor
            preprocessing_states: preprocessing state features for better representation
            mdp: instance of environment, allowing to simulate action with self.mdp.step_from_state(current_state, action)
            theta: linear weight (function approximation)
        """
        self.gamma = 0.9
        n_components = 10 # You can change
        self.preprocessing_states = Pipeline([("scaler", StandardScaler()), ("feature_generation", RBFSampler(n_components=n_components))])
        self.preprocessing_states.fit(np.array([[np.random.uniform(-1.2, 0.6), np.random.uniform(-0.07, 0.07)] for _ in range(10000)]))
        self.mdp = Mountain()
        self.theta = np.zeros(n_components) # Linear weight

    def preprocessing(self, state):
        """
        Return the featurized representation of a state.
        """
        return self.preprocessing_states.transform([state])[0]

    def act(self, state):
        """Output optimal action of a given state
        Returns: action in [0, 1, 2]
        """

        Vs = np.array([self.v(self.mdp.step_from_state(state, action)[0]) for action in [0, 1, 2]])
        idx_best = np.argsort(-Vs)

        if np.random.uniform(0, 1) < 0.1:
            return np.random.choice(list(idx_best[1:]))
        else:
            return idx_best[0]

    def update(self, state, action, reward, new_state, terminal):
        """Receive a reward after performing given action.

        This is where your agent can learn. (Update theta.)
        Parameters:
            state: current state
            action: action done in state
            reward: reward received after doing action in state
            new_state: next state
            terminal: boolean if new_state is a terminal state or not
        """
        if terminal:
            self.theta = self.theta + 1e-3 * (reward - self.v(state)) * self.preprocessing(state)
        else:
            done = False
            s = new_state
            i = 1
            target = reward
            while (not done) and (i < 30):
                s, r, done = self.mdp.step_from_state(s, self.act(s))
                target += r * self.gamma**i
                i += 1

            self.theta = self.theta + 1e-3 * (target - self.v(state)) * self.preprocessing(state)

    def v(self, state):
        """Final V function.
        Return:
            Value (scalar): V(state)
        """
        return np.dot(self.preprocessing(state), self.theta)



class TD0_State_Value_Approximation:
    """ TD(0) State-value Learning with Function Approximation
    """

    def __init__(self):
        """
        Init:
            gamma: discount factor
            preprocessing_states: preprocessing state features for better representation
            mdp: instance of environment, allowing to simulate action with self.mdp.step_from_state(current_state, action)
            theta: linear weight (function approximation)
        """
        self.gamma = 0.9
        n_components = 10 # You can change
        self.preprocessing_states = Pipeline([("scaler", StandardScaler()), ("feature_generation", RBFSampler(n_components=n_components))])
        self.preprocessing_states.fit(np.array([[np.random.uniform(-1.2, 0.6), np.random.uniform(-0.07, 0.07)] for _ in range(10000)]))
        self.mdp = Mountain()
        self.theta = np.zeros(n_components) # Linear weight

    def preprocessing(self, state):
        """
        Return the featurized representation of a state.
        """
        return self.preprocessing_states.transform([state])[0]

    def act(self, state):
        """Output optimal action of a given state
        Return: action in [0, 1, 2]
        """
        # TO IMPLEMENT
        return np.random.choice([0, 1, 2])


    def update(self, state, action, reward, new_state, terminal):
        """Receive a reward after performing given action.

        This is where your agent can learn. (Update theta.)
        Input:
            state: current state
            action: action done in state
            reward: reward received after doing action in state
            new_state: next state
            terminal: boolean if new_state is a terminal state or not
        """
        # TO IMPLEMENT
        pass

    def v(self, state):
        """Final V function.
        Return:
            Value (scalar): V(state)
        """
        # TO IMPLEMENT
        return np.random.uniform(0, 1)



class n_steps_TD_State_Value_Approximation:
    """ TD(lambda) State-value Learning with Function Approximation
    http://www.incompleteideas.net/book/7/node2.html
    """

    def __init__(self):
        """
        Init:
            gamma: discount factor
            preprocessing_states: preprocessing state features for better representation
            mdp: instance of environment, allowing to simulate action with self.mdp.step_from_state(current_state, action)
            theta: linear weight (function approximation)
            lambda_value: Monte-Carlo step
        """
        self.gamma = 0.9
        n_components = 10 # You can change
        self.preprocessing_states = Pipeline([("scaler", StandardScaler()), ("feature_generation", RBFSampler(n_components=n_components))])
        self.preprocessing_states.fit(np.array([[np.random.uniform(-1.2, 0.6), np.random.uniform(-0.07, 0.07)] for _ in range(10000)]))
        self.mdp = Mountain()
        self.theta = np.zeros(n_components) # Linear weight
        self.mc_steps = 5 # Monte Carlo steps

    def preprocessing(self, state):
        """
        Return the featurized representation of a state.
        """
        return self.preprocessing_states.transform([state])[0]

    def act(self, state):
        """Output optimal action of a given state
        Return: action in [0, 1, 2]
        """
        # TO IMPLEMENT
        return np.random.choice([0, 1, 2])


    def update(self, state, action, reward, new_state, terminal):
        """Receive a reward after performing given action.

        This is where your agent can learn. (Update theta.)
        Input:
            state: current state
            action: action done in state
            reward: reward received after doing action in state
            new_state: next state
            terminal: boolean if new_state is a terminal state or not
        """
        # TO IMPLEMENT
        pass

    def v(self, state):
        """Final V function.
        Return:
            Value (scalar): V(state)
        """
        # TO IMPLEMENT
        return np.random.uniform(0, 1)



class TD0_Q_Learning_Function_Approximation:
    """ Q-Learning with Function Approximation
    """

    def __init__(self):
        """
        Init:
            gamma: discount factor
            preprocessing_states: preprocessing state features for better representation
            mdp: instance of environment, allowing to simulate action with self.mdp.step_from_state(current_state, action)
            theta: linear weight (function approximation)
        """
        self.gamma = 0.9
        n_components = 10
        self.preprocessing_states = Pipeline([("scaler", StandardScaler()), ("feature_generation", RBFSampler(n_components=n_components))])
        self.preprocessing_states.fit(np.array([[np.random.uniform(-1.2, 0.6), np.random.uniform(-0.07, 0.07)] for _ in range(10000)]))
        self.mdp = Mountain()
        self.theta = None # TO IMPLEMENT

    def preprocessing(self, state):
        """
        Return the featurized representation of a state.
        """
        return self.preprocessing_states.transform([state])[0]

    def act(self, state):
        """Output optimal action of a given state
        Return: action in [0, 1, 2]
        """
        # TO IMPLEMENT
        return np.random.choice([0, 1, 2])

    def update(self, state, action, reward, new_state, terminal):
        """Receive a reward for performing given action.

        This is where your agent can learn. (Update self.theta)
        Input:
            state: current state
            action: action done in state
            reward: reward received after doing action in state
            new_state: next state
            terminal: boolean if new_state is a terminal state or not
        """
        pass


    def q(self, state, action):
        """Final Q function.
            Value (scalar): Q(state, action)
        """
        # TO IMPLEMENT
        return np.random.uniform(0, 1)



class Direct_Policy_Search_Agent:
    def __init__(self):
        """
        Init a new agent.
        """
        pass

    def train(self):
        """
        Learn the policy.
        """

        ############### EXAMPLE ########

        # 1- Define state features
        # state = [position, velocity]

        # 2- Define search space (of a policy)
        policy = np.zeros(2) # R**2

        # 3- Define objective function (to assess a policy)
        def objective_function(policy):
            total = 0
            env = Mountain()
            state = env.observe()
            done = False
            while not done:
                action = np.random.choice([0, 1, 2]) # random action :/
                state, reward, done = env.step_from_state(state, action)
                total += -1
            return - total # loss

        # 4- Optimize the objective function (using cma.fmin2)
        # 5- Save optimal policy; and use in `act` method
        self.policy_opt, _ = cma.fmin2(objective_function, policy, 0.5)


    def act(self, observation):
        """Output optimal action of a given state
            Return: action in [0, 1, 2]
        """
        return np.random.choice([0, 1, 2])
