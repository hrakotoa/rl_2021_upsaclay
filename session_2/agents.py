"""
Angents file. TO BE COMPLETED
"""
import numpy as np
import math


class PolicyIteration:
    def __init__(self, mdp):
        self.mdp = mdp
        self.gamma = 0.9

    def policy_evaluation(self, policy):
        """1st step of policy iteration algorithm
            Input:
                policy: current policy
            Return: State Value V of policy
        """
        V = np.zeros(self.mdp.env.nS) # intialize V to 0's

        # TO IMPLEMENT

        return np.array(V)

    def policy_improvement(self, V, policy):
        """2nd step of policy iteration algorithm
            Input:
                V: improved V
                policy: current policy
            Return: the improved policy
        """
        # TO IMPLEMENT

        return policy


    def policy_iteration(self):
        """This is the main function of policy iteration algorithm.
            Return:
                final policy
                (optimal) state value function V
        """
        # Start with a random policy
        policy = np.ones((self.mdp.env.nS, self.mdp.env.nA)) / self.mdp.env.nA
        # Action in [UP, RIGHT, DOWN, LEFT], see environment.py
        V = np.zeros(self.mdp.env.nS)

        # TO IMPLEMENT: call iteratively step 1 and 2 until convergence
        return policy, V


class ValueIteration:
    def __init__(self, mdp):
        self.mdp = mdp
        self.gamma = 0.9

    def optimal_value_function(self):
        """1st step of value iteration algorithm
            Return: State Value V
        """
        # Initialize random policy and V
        policy = np.ones((self.mdp.env.nS, self.mdp.env.nA)) / self.mdp.env.nA
        V = np.zeros(self.mdp.env.nS)

        # TO IMPLEMENT
        return V

    def optimal_policy_extraction(self, V):
        """2nd step of value iteration algorithm
            Input:
                V: optimal state values
            Return: optimal policy
        """
        policy = np.zeros([self.mdp.env.nS, self.mdp.env.nA])
        # TO IMPLEMENT

        return policy

    def value_iteration(self):
        """This is the main procedure of value iteration algorithm.
            Return:
                final policy
                (optimal) state value function V
        """

        # TO IMPLEMENT
        return policy, V


class QLearning:
    def __init__(self, mdp):
        self.q_table = np.zeros((4, mdp.env.observation_space.n))
        self.mdp = mdp
        self.discount = 0.9

    def update(self, state, action, reward):
        """
        Update Q-table according to previous state (observation), current state, action done and obtained reward.
            Input:
                state: state s(t), before moving according to 'action'
                action: action a(t) moving from state s(t) (='state') to s(t+1)
                reward: reward received after achieving 'action' from state 'state'
            Return: None
        """
        new_state = self.mdp.observe() # To get the new current state

        # TO IMPLEMENT
        raise NotImplementedError

    def action(self, state):
        """
        Find which action to do given a state.
            Input:
                state: state observed at time t, s(t)
            Return:
                action: optimal action a(t) to run
        """
        # TO IMPLEMENT
        raise NotImplementedError
        return action


class Agent(object):
    """Agent base class. DO NOT MODIFY THIS CLASS
    """

    def __init__(self, mdp):
        super(Agent, self).__init__()
        self.mdp = mdp


    def update(self, state, action, reward):
        # DO NOT MODIFY. This is an example
        pass

    def action(self, state):
        # DO NOT MODIFY. This is an example
        return self.mdp.env.action_space.sample()
