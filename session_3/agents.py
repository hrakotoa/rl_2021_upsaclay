import numpy as np
import random


"""
Contains the definition of the agent that will run in an
environment.
"""

class UniformBandit:
    def __init__(self, arms):
        """Init.
        """
        self.arms = arms

    def choose(self):
        """ Choose the most promising arm.
            Return: arm index
        """
        return np.random.randint(0, len(self.arms))

    def update(self, arm, reward):
        """Receive a reward after choosing a particular arm.
        This is where the agent can learn.
            Input:
                arm: choosed arm
                reward: feedback value associated with arm
        """
        pass

class epsGreedyBandit:
    def __init__(self, arms):
        """Initialize the set of arms.
        """
        self.arms = arms

    def choose(self):
        """ Choose the most promising arm.
            Return: arm index
        """
        raise NotImplementedError()

    def update(self, arm, reward):
        """Receive a reward after choosing a particular arm.
        This is where the agent can learn.
            Input:
                arm: choosed arm
                reward: feedback value associated with arm
        """
        raise NotImplementedError()


class UCB1Bandit:
    # https://homes.di.unimi.it/~cesabian/Pubblicazioni/ml-02.pdf
    def __init__(self, arms):
        """Initialize the set of arms.
        """
        self.arms = arms

    def choose(self):
        """ Choose the most promising arm.
            Return: arm index
        """
        raise NotImplementedError()

    def update(self, arm, reward):
        """Receive a reward after choosing a particular arm.
        This is where the agent can learn.
            Input:
                arm: choosed arm
                reward: feedback value associated with arm
        """
        raise NotImplementedError()

class ThompsonBandit:
    # https://web.stanford.edu/~bvr/pubs/TS_Tutorial.pdf, chapter 3
    def __init__(self, arms):
        """Initialize the set of arms.
        """
        self.arms = arms

    def choose(self):
        """ Choose the most promising arm.
            Return: arm index
        """
        # You can use np.random.beta
        raise NotImplementedError()

    def update(self, arm, reward):
        """Receive a reward after choosing a particular arm.
        This is where the agent can learn.
            Input:
                arm: choosed arm
                reward: feedback value associated with arm
        """
        raise NotImplementedError()


class BesaBandit():
    # https://hal.archives-ouvertes.fr/hal-01025651v1/document
    def __init__(self, arms):
        """Initialize the set of arms.
        """
        self.arms = arms

    def choose(self):
        """ Choose the most promising arm.
            Return: arm index
        """
        raise NotImplementedError()

    def update(self, arm, reward):
        """Receive a reward after choosing a particular arm.
        This is where the agent can learn.
            Input:
                arm: choosed arm
                reward: feedback value associated with arm
        """
        raise NotImplementedError()


class KLUCBBandit:
    # See: https://arxiv.org/pdf/1102.2490.pdf
    def __init__(self, arms):
        """Initialize the set of arms.
        """
        self.arms = arms

    def choose(self):
        """ Choose the most promising arm.
            Return: arm index
        """
        raise NotImplementedError()

    def update(self, arm, reward):
        """Receive a reward after choosing a particular arm.
        This is where the agent can learn.
            Input:
                arm: choosed arm
                reward: feedback value associated with arm
        """
        raise NotImplementedError()
